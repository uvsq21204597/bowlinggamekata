# README #

Exercice Bowling Game Kata

Objectifs de l'exercice:

Sur la forge, créez un nouveau dépôt pour le projet (avec un README et un .gitignore pour
Maven).
— Clonez localement ce projet avec Git.
— Générez le squelette du projet Maven à l’aide de l’archetype maven-archetype-quickstart (cf.
Maven in 5 Minutes).
— Vérifiez que le projet se construit correctement.
— Validez les changements et synchroniser avec la forge.
— Mettez à jour la dépendance avec JUnit vers la version la plus récente (cf. MVNRepository) et
modifier le code de test en conséquence (cf. A Simple Unit Test).
— Vérifiez que le projet se construit correctement, validez et synchronisez.

Dans cet exercice, vous vous familiariserez avec le développement dirigé par les tests (Test Driven Deve-
lopment ou TDD). Pour cela, vous suivrez les étapes du document The Bowling Game Kata, Robert C.
Martin.